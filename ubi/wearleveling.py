
import heapq
class ReplaceLowestEraseCount:
    def execute(self, device, freeBlocks, usedBlocks):
        leastUsedIndex = usedBlocks.index(min(usedBlocks, key = lambda block: block.eraseCount()))
        device.write(leastUsedIndex)
