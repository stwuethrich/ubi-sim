

class Simulator:
    def __init__(self, device):
        self._device = device

    def run(self, writer, numberOfRuns):
        for i in range(numberOfRuns):
            writer.execute(self._device)

    def runWithWearleveling(self, writer, wearleveler, numberOfRuns):
        for i in range(numberOfRuns):
            writer.execute(self._device)
            self._device.wearlevel(wearleveler)
        
