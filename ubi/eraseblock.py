class EraseBlock:

    def __init__(self):
        EraseBlock.totalSequenceCount = 0
        self._sequenceCount = 0
        self._eraseCount = 0

    def write(self):
        EraseBlock.totalSequenceCount = EraseBlock.totalSequenceCount + 1
        self._sequenceCount = EraseBlock.totalSequenceCount

    def erase(self):
        self._eraseCount = self._eraseCount + 1

    def print(self):
        print(" {:6d},  {:6d}".format(self._eraseCount, self._sequenceCount))

    def eraseCount(self):
        return self._eraseCount

    def sequenceCount(self):
        return self._sequenceCount
