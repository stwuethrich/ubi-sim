from typing import Sequence
from ubi.eraseblock import EraseBlock

class Device:
    def __init__(self, numberOfBlocks, volumeSize):
        self._numberOfBlocks = numberOfBlocks
        self._volumeSize = volumeSize
        self._freeBlocks = []
        self._volume = []
    
        for i in range(numberOfBlocks):
            self._freeBlocks.append(EraseBlock())

        # create "volume"
        for i in range(self._volumeSize):
            block = self._freeBlocks.pop()
            block.write()
            self._volume.append(block)
        
    def write(self, index):
        self._eraseBlock(index)
        self._assignNewBlock(index)

    def wearlevel(self, wearleveler):
        wearleveler.execute(self, self._freeBlocks, self._volume)

    def allBlocks(self):
        return (self._freeBlocks + self._volume)

    def eraseCountList(self):
        result = [block.eraseCount() for block in self._freeBlocks + self._volume]
        return result

    def sequenceCountList(self):
        result = [block.sequenceCount() for block in self._freeBlocks + self._volume]
        return result


    def print(self):
        print("free bocks:")
        for block in self._freeBlocks:
            block.print()
        print("in use:")
        for block in self._volume:
            block.print()

    def printAll(self):
        print("all blocks:")
        for block in self.allBlocks():
            block.print()

    def _eraseBlock(self, index):
        old = self._volume[index]
        old.erase()
        self._freeBlocks.append(old)
        self._freeBlocks.sort(key = lambda block: block.eraseCount(), reverse=True)

    def _assignNewBlock(self, index):
        new = self._freeBlocks.pop()
        new.write()
        self._volume[index] = new