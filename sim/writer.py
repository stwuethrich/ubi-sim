from random import randrange

class WriteOneAddress:
    def __init__(self, address):
        self._address = address
    
    def execute(self, device):
        device.write(self._address)

class WriteRange:
    def __init__(self, beginIndex, endIndex):
        self._begin = beginIndex
        self._end = endIndex
    
    def execute(self, device):
        for index in range(self._begin, self._end):
            device.write(index)

class WriteRandomAddress:
    def __init__(self, rangeBegin, rangeEnd):
        self._begin = rangeBegin
        self._end = rangeEnd

    def execute(self, device):
        index = randrange(self._begin, self._end)
        device.write(index)

class WriteMultiple:
    def __init__(self, writers):
        self._writers = writers

    def execute(self, device):
        for writer in self._writers:
            writer.execute(device)